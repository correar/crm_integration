# crm_integration

Integration project between Pipedrive and bling

Packages:

```
falcon
gunicorn
pymongo
pytest
pipedrive-python-lib
json2xml
```

## How to Run

- Clone the project

- Create a env file `.env`

Fill with these envs

```
PIPEDRIVE_API_TOKEN={Your pipdrive api token}
PIPEDRIVE_DOMAIN={Your pipdrive api domain}
PIPEDRIVE_FILTER_ID={Create a filter in pipedriver to list all deals with status "ganho"}
BLING_API_TOKEN={Your Bling Token}
BLING_DOMAIN={Your Bling Domain}
DATABASE_USER={Mongo User}
DATABASE_PASSWORD={Mongo password}
DATABASE_URL=crm_integration_mongo_1
DATABASE_PORT=27017
DATABASE_NAME=crm
```

Use docker compose with the command:

```
sudo docker-compose up --build app
```

**What it will go happens?**

Docker will install python3 and all packages

It will be install Mongo in crm_integration_mongo_1 on port 27017 with database name `crm`, use any mongo IDE to view it working

**Accessing the endpoint**

Open your [postman](https://www.postman.com/) or [insominia](https://insomnia.rest/download/)

Create a GET endpoint

```
host = http://localhost:5000/deals
```

and send

**What it does?**

- The application will connect in pipedrive get all deals with status "ganho"
- If it has new deals,, The application will connect in bling and create a new order
- and it will save in mongo database the id, date and deal value
- The application will return the total amount aggregate by day

Eg. Response:

Status Code: 200

```
[
  {
    "date": "2020-12-07",
    "totalAmount": 600.5,
    "totalDeals": 2
  },
  {
    "date": "2020-12-05",
    "totalAmount": 200,
    "totalDeals": 1
  }
]
```

# How to Run Test

Run docker compose command:

```
sudo docker-compose up --build test
```

Eg. Result:

```
test_1   | ============================= test session starts ==============================
test_1   | platform linux -- Python 3.8.0, pytest-6.1.2, py-1.9.0, pluggy-0.13.1
test_1   | rootdir: /code
test_1   | collected 1 item
test_1   |
test_1   | tests/test_app.py .                                                      [100%]
test_1   |
test_1   | ========================= 1 passed, 1 warning in 1.07s =========================
```
