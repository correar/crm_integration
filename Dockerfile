FROM python:3

WORKDIR /code

COPY requirements.txt ./

RUN apt-get update && \
    apt-get install -y python-pip python-dev build-essential \
    gunicorn

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

COPY . .

CMD ["gunicorn", "--bind", ":5000", "--workers", "3", "app.main:api"]