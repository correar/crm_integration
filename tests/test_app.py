import falcon
from falcon import testing
import msgpack
import pytest
import json

from app.main import api

@pytest.fixture
def client():
    return testing.TestClient(api)

def test_deals(client):
    doc = [
        {
            "date": "2020-12-07",
            "totalAmount": 600.5,
            "totalDeals": 2
        },
        {
            "date": "2020-12-05",
            "totalAmount": 200,
            "totalDeals": 1
        }
        
    ]
    response = client.simulate_get('/deal')
    result_doc = json.loads(response.text)
    
    assert result_doc == doc
    assert response.status == falcon.HTTP_OK