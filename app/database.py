from pymongo import MongoClient
import os

class Mongo:
    def connection():
        client = MongoClient('mongodb://%s:%s@%s:%s' % (os.environ.get('DATABASE_USER'), os.environ.get('DATABASE_PASSWORD'), os.environ.get('DATABASE_URL'), os.environ.get('DATABASE_PORT') ))
        return client['%s' % os.environ.get('DATABASE_NAME')]
    
    def insert_one(data, collection):    
        db = Mongo.connection()
        collection = db[collection]
        payload = {
            'codigo': data["id"],
            'valor': data["value"]
        }
        collection.insert_one(data)

    def find(collection):
        db = Mongo.connection()
        collection = db[collection]
        return collection.find()

    def find_total_value_by_day(collection):
        db = Mongo.connection()
        collection = db[collection]
        values = [{
            "$group": {
                "_id": {"date": "$date"},
                "totalAmount": {"$sum": "$value"},
                "count": {"$sum": 1}
            }
        }]
        results = collection.aggregate(values)
        responses = []
        for result in list(results):
            data = {
                "date": result["_id"]["date"],
                "totalAmount": result["totalAmount"],
                "totalDeals": result["count"]
            }
            responses.append(data)
        return responses