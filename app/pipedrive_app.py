from pipedrive.client import Client
import os

client = Client(domain=os.environ.get('PIPEDRIVE_DOMAIN'))
client.set_api_token(os.environ.get('PIPEDRIVE_API_TOKEN'))

class Pipedrive:
    def get_deal_by_day():
        deals = client.deals.get_all_deals_with_filter(os.environ.get('PIPEDRIVE_FILTER_ID'))
        return deals
        