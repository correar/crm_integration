import json
import falcon
import logging
from .pipedrive_app import Pipedrive
from .bling_app import Bling
from .database import Mongo

class Resource(object):
    def on_get(self, req, resp):
        try:
            pipedrive_response = Pipedrive.get_deal_by_day()
            deals = Mongo.find('deal_done')
            if (len(pipedrive_response["data"]) > deals.count()):
                max_size = deals.count() - len(pipedrive_response["data"])
                bling_order_response = Bling.post_order(pipedrive_response["data"][max_size:])
            total_deals = Mongo.find_total_value_by_day('deal_done')
            resp.body = json.dumps(total_deals)
            resp.status = falcon.HTTP_200
        except Exception as error:
            resp.body = json.dumps({"error": f"{error}"})
            resp.status = falcon.HTTP_400
        