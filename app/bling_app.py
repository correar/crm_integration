import requests
import os
import json
from json2xml import json2xml
from json2xml.utils import readfromstring
from .database import Mongo

class Bling(object):
    def get_date(date):
        date_split_space = date.split(" ")
        date_split_dash = date_split_space[0].split("-")
        return date_split_dash[2]+"/"+date_split_dash[1]+"/"+date_split_dash[0]
        
    def parse_to_bling(deals):
        deal_list = []
        for deal in deals:
            id = deal["id"]
            date = deal["add_time"]
            person_id = deal["person_id"]
            customer_name = person_id["name"]
            item_description = deal["title"]
            value = deal["value"]
            date_format = Bling.get_date(date)
            xml = '<pedido><data>%s</data><numero>%s</numero><cliente><nome>%s</nome></cliente><items><item><codigo>%s</codigo><descricao>%s</descricao><qtde>1</qtde><vlr_unit>%f</vlr_unit></item></items></pedido>' % (date_format, id, customer_name, id, item_description, value)
            data = {
                'id': id,
                'date': date.split(" ")[0],
                'value': value
            }
            resp = {"xml": xml, "data": data}
            deal_list.append(resp)
        return deal_list

    def post_order(data):
        payloads = readfromstring(json.dumps(Bling.parse_to_bling(data)))
        url = os.environ.get('BLING_DOMAIN') + 'pedido/json/&apikey=%s' % os.environ.get('BLING_API_TOKEN')
        bling_response = []
        for payload in payloads:
            bling = requests.post(url=url, data={'apikey':os.environ.get('BLING_API_TOKEN'),'xml':payload["xml"]})
            bling_response.append(json.loads(bling.text))
            Mongo.insert_one(payload["data"], "deal_done")
        return bling_response
        

    
    