import falcon
from .deals import Resource

api = application = falcon.API()
deals = Resource()
api.add_route('/deals', deals)